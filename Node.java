/*
 * Class for a node in binary search tree
 * @Author Andrew Greaves
 */
public class Node<E> {
	
    private Node<E> left,right,parent;
	private E element;
	 
	//Constructors
	public Node(E element) {
		left=null;
		right=null;
		parent=null;
		this.element=element;
	}
	
	public Node() {
		left=null;
		right=null;
		parent=null;
		this.element=null;
	}

	//Getters and Setters
	public Node<E> getLeft() {
		return left;
	}

	public void setLeft(Node<E> left) {
		this.left = left;
	}

	public Node<E> getRight() {
		return right;
	}

	public void setRight(Node<E> right) {
		this.right = right;
	}

	public Node<E> getParent() {
		return parent;
	}

	public void setParent(Node<E> parent) {
		this.parent = parent;
	}

	public E getElement() {
		return element;
	}

	public void setElement(E element) {
		this.element = element;
	}
	
	//Determines if a given node is internal
	public boolean isInternal() {
		if (this.getElement()!=null)
			return true;
		else return false;
	}
	
	//Determines if a given node is a leaf
	public boolean isLeaf() {
		if (this.getElement()==null)
			return true;
		else return false;
	}
	

}
