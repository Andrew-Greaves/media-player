/*
 * Class which implements a binary search tree using an ordered dictionary
 * @Author Andrew Greaves
 */
public class OrderedDictionary implements OrderedDictionaryADT {
	private Node<Record> root;

	// Constructor
	public OrderedDictionary() {
		this.root = new Node<Record>();
	}

	// Gets a record from the dictionary
	public Record get(Pair k) {
		boolean found = false;
		Node<Record> p = getRoot();
		if (p.getElement() == null)
			return null;
		else {
			while (found == false) {
				if (p.getElement().getKey().compareTo(k) == 0) {
					found = true;
					return p.getElement();
				} else if (p.getElement().getKey().compareTo(k) == -1) {
					p = p.getRight();
					if (p.isLeaf())
						return null;
				} else {
					p = p.getLeft();
					if (p.isLeaf())
						return null;
				}
			}
			return null;
		}
	}

	// Puts a record in the dictionary
	public void put(Record r) throws DictionaryException {
		Node<Record> p = getRoot();
		Node<Record> w = new Node<Record>();
		Node<Record> v = new Node<Record>();
		Node<Record> q = getRoot();
		if (get(r.getKey()) != null)
			throw new DictionaryException("The record is already in the dictionary");
		else {
			if (getRoot().getElement() == null) {
				getRoot().setElement(r);
				getRoot().setLeft(new Node<Record>());
				getRoot().setRight(new Node<Record>());
			} else {
				boolean inserted = false;
				while (inserted == false) {
					if (p.getElement().getKey().compareTo(r.getKey()) == -1) {
						p = p.getRight();
						p.setParent(q);
						q = p;
						if (p.isLeaf()) {
							p.setElement(r);
							p.setLeft(w);
							w.setParent(p);
							p.setRight(v);
							v.setParent(p);
							inserted = true;
						}
					} else {
						p = p.getLeft();
						p.setParent(q);
						q = p;
						if (p.isLeaf()) {
							p.setElement(r);
							p.setLeft(w);
							w.setParent(p);
							p.setRight(v);
							v.setParent(p);
							inserted = true;
						}
					}
				}
			}
		}
	}

	// Removes a record from the dictionary
	public void remove(Pair k) throws DictionaryException {
		Node<Record> p1, child;
		Node<Record> p = getNode(k);
		if (p.getElement() == null) {
			throw new DictionaryException("Key not in dictionary");
		} else {
			if ((p.getLeft().isLeaf()) || (p.getRight().isLeaf())) {
				p1 = p.getParent();
				if (p.getLeft().isInternal()) {
					child = p.getLeft();
				} else {
					child = p.getRight();
				}
				if (p == getRoot())
					setRoot(child);
				else if (p1.getLeft() == p)
					p1.setLeft(child);
				else
					p1.setRight(child);
			} else {
				Node<Record> s = smallestFrom(p.getRight());
				p.setElement(s.getElement());
				s.setElement(null);
			}

		}
	}

	// Finds and returns the successor of a record in the dictionary
	public Record successor(Pair k) {
		if (getRoot().isLeaf())
			return null;
		else {
			Node<Record> p = getNode(k);
			if ((p.isInternal()) && (p.getRight().isInternal()))
				return smallestFrom(p.getRight()).getElement();
			else {
				Node<Record> p1 = p.getParent();
				while ((p != getRoot()) && (p1.getRight() == p)) {
					p = p1;
					p1 = p.getParent();
				}
				if (p == getRoot())
					return null;
				else
					return p1.getElement();
			}
		}
	}

	// Finds and returns the predecessor of a record in the dictionary
	public Record predecessor(Pair k) {
		if (getRoot().isLeaf())
			return null;
		else {
			Node<Record> p = getNode(k);
			if ((p.isInternal()) && (p.getLeft().isInternal()))
				return largestFrom(p.getLeft()).getElement();
			else {
				Node<Record> p1 = p.getParent();
				while ((p != getRoot()) && (p1.getLeft() == p)) {
					p = p1;
					p1 = p.getParent();
				}
				if (p == getRoot())
					return null;
				else
					return p1.getElement();
			}
		}
	}

	// Returns the smallest record in the dictionary
	public Record smallest() {
		if (getRoot().getElement() == null)
			return null;
		else {
			Node<Record> p = getRoot();
			while (p.isInternal()) {
				if (p.getLeft().isLeaf())
					return p.getElement();
				else
					p = p.getLeft();
			}
			return p.getParent().getElement();
		}
	}

	// Returns the largest record in the dictionary
	public Record largest() {
		if (getRoot().getElement() == null)
			return null;
		else {
			Node<Record> p = getRoot();
			while (p.isInternal()) {
				if (p.getRight().isLeaf())
					return p.getElement();
				else
					p = p.getRight();
			}
			return p.getParent().getElement();
		}
	}

	// Private method which returns a node given a key
	private Node<Record> getNode(Pair k) {
		boolean found = false;
		Node<Record> p = getRoot();
		if (p.getElement() == null)
			return null;
		else {
			while (found == false) {
				if (p.getElement().getKey().compareTo(k) == 0) {
					found = true;
					return p;
				} else if (p.getElement().getKey().compareTo(k) == -1) {
					p = p.getRight();
					if (p.isLeaf())
						return p;
				} else {
					p = p.getLeft();
					if (p.isLeaf())
						return p;
				}
			}
			return null;
		}
	}

	// Gets the root of the binary search tree
	private Node<Record> getRoot() {
		return root;
	}

	// Sets the root of the binary search tree
	private void setRoot(Node<Record> newRoot) {
		root = newRoot;
	}

	// Finds the smallest node starting from a given node
	private Node<Record> smallestFrom(Node<Record> p) {
		while (p.isInternal()) {
			if (p.getLeft().isLeaf())
				return p;
			else
				p = p.getLeft();
		}
		return p.getParent();
	}

	// Finds the largest node starting from a given node
	private Node<Record> largestFrom(Node<Record> p) {
		while (p.isInternal()) {
			if (p.getRight().isLeaf())
				return p;
			else
				p = p.getRight();
		}
		return p.getParent();
	}

}
