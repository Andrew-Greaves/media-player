/*
 * Pair class
 * @Author Andrew Greaves
 */
public class Pair {
	
	private String word,type;
	
	//Constructor
	public Pair(String word,String type) {
		this.word=word;
		this.type=type;
	}

	//Getters
	public String getWord() {
		return word;
	}

	public String getType() {
		return type;
	}
	
	//Function to compare two Pairs
	public int compareTo(Pair k) {
		if(getWord().equals(k.getWord()) && getType().equals(k.getType()))
			return 0;
		else if((getWord().compareTo(k.getWord())<0) || (getWord().equals(k.getWord()) && (getType().compareTo(k.getType())<0)))
				return -1;
		else return 1;
	}

}
