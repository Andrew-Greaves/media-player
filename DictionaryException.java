/*
 * Class for Dictionary Exception
 * @Author Andrew Greaves
 */
public class DictionaryException extends RuntimeException {
	
	public DictionaryException(String message) {
		super(message);
	}

}