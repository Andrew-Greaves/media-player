
/*
 * User interface of binary search tree
 * @Author Andrew Greaves
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class UserInterface {
	// Reads input from file into ordered dictionary
	public static void main(String[] args) {
		BufferedReader in;
		String word;
		OrderedDictionary dictionary = new OrderedDictionary();
		StringReader keyboard = new StringReader();
		PictureViewer picture = new PictureViewer();
		SoundPlayer sound = new SoundPlayer();
		try {
			in = new BufferedReader(new FileReader(args[0]));
			word = in.readLine();
			word = word.toLowerCase();

			String definition;
			while (word != null) {
				try {
					definition = in.readLine();
					if (definition.endsWith(".wav") || definition.endsWith(".mid"))
						dictionary.put(new Record(new Pair(word, "audio"), definition));
					else if (definition.endsWith(".jpg") || definition.endsWith(".gif"))
						dictionary.put(new Record(new Pair(word, "image"), definition));
					else
						dictionary.put(new Record(new Pair(word, "text"), definition));
					word = in.readLine();
					word = word.toLowerCase();
				} catch (Exception e) {
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Record rec, rec1;
		// Gets user input, and splits it by word
		String line = keyboard.read("Enter next command: ");
		String[] splited = line.split(" ");
		while (!splited[0].equals("finish")) {
			boolean found = false;
			// Checks if user entered command get, returns the record in the tree that the
			// user requested, if it is in the tree
			if (splited[0].equals("get")) {
				rec = dictionary.get(new Pair(splited[1], "image"));
				if (rec != null) {
					found = true;
					try {
						picture.show(rec.getData());
					} catch (MultimediaException e) {
						e.printStackTrace();
					}
				}
				rec = dictionary.get(new Pair(splited[1], "audio"));
				if (rec != null) {
					found = true;
					try {
						sound.play(rec.getData());
					} catch (MultimediaException e) {
						e.printStackTrace();
					}
				}
				rec = dictionary.get(new Pair(splited[1], "text"));
				if (rec != null) {
					found = true;
					System.out.println(rec.getData());
				}
				if (found == false) {
					System.out.println("The word " + splited[1] + " is not in dictionary");
					rec = dictionary.predecessor(new Pair(splited[1], "text"));
					if (rec == null)
						System.out.println("Preceeding word: ");
					else
						System.out.println("Preceeding word: " + rec.getKey().getWord());
					rec = dictionary.successor(new Pair(splited[1], "text"));
					if (rec == null)
						System.out.println("Following word: ");
					else
						System.out.println("Following word " + rec.getKey().getWord());
				}

				// Removes a record from the tree, if it is in the tree
			} else if (splited[0].equals("delete")) {
				try {
					dictionary.remove(new Pair(splited[1], splited[2]));
				} catch (DictionaryException e) {
					System.out.println("No record in the dictionary has key (" + splited[1] + "," + splited[2] + ")");
				}

				// Adds a record to the tree, if it is not already in it
			} else if (splited[0].equals("put")) {
				try {
					String data = "";
					for (int i = 3; i < splited.length; i++) {
						data = data + " " + splited[i];
					}
					dictionary.put(new Record(new Pair(splited[1], splited[2]), data));
				} catch (DictionaryException e) {
					System.out.println("A record with the given key " + splited[1] + "," + splited[2]
							+ " is already in the ordered dictionary");
				}

				// Lists all the records in the tree beginning with a given prefix
			} else if (splited[0].equals("list")) {
				rec = dictionary.get(new Pair(splited[1], "text"));
				if (rec != null)
					System.out.print(rec.getKey().getWord() + ",");
				rec1 = dictionary.successor(new Pair(splited[1], "text"));
				if (rec == null && rec1 == null)
					System.out.println("No record in the ordered dictionary start with prefix " + splited[1]);
				while (rec1 != null) {
					if (rec1.getKey().getWord().startsWith(splited[1]))
						System.out.print(rec1.getKey().getWord() + ",");
					rec1 = dictionary.successor(new Pair(rec1.getKey().getWord(), rec1.getKey().getType()));
				}
				System.out.println();

				// Returns the smallest record in the tree
			} else if (splited[0].equals("smallest")) {
				rec = dictionary.smallest();
				System.out.println(
						rec.getKey().getWord() + "," + rec.getKey().getType() + "," + rec.getData().toString());

				// Returns the largest record in the tree
			} else if (splited[0].equals("largest")) {
				rec = dictionary.largest();
				System.out.println(
						rec.getKey().getWord() + "," + rec.getKey().getType() + "," + rec.getData().toString());
			} else
				System.out.println("Incorrect command entered");
			line = keyboard.read("Enter next command: ");
			splited = line.split(" ");
		}
		// Terminates program
		if (splited[0].equals("finish")) {
			System.exit(0);
		}
	}

}
