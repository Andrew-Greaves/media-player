/*
 * Class to store a Record
 * @Author Andrew Greaves
 */
public class Record {
	private Pair key;
	private String data;
	
	//Constructor
	public Record(Pair key,String data) {
		this.key=key;
		this.data=data;
		if((key.getType().equals("audio"))|| (key.getType().equals("image")))
			data=key.getWord();
	}

	//Getters
	public Pair getKey() {
		return key;
	}

	public String getData() {
		return data;
	}
	
	

}
